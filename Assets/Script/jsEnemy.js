#pragma strict

private var power = 1200; // Turret Shot Speed

var bullet : Transform; // Bullet
var target : Transform; // LookAt() target
var spPoint : Transform; // spawnPoint
var explosion : Transform; // spawnPoint Explosion
var snd : AudioClip; // ShotSound

private var ftime : float = 0.0; // Shot Time Limit

function Update () {
	transform.LookAt(target); // ally direction rotate
	ftime += Time.deltaTime; // Time acumullate
	
	var hit : RaycastHit; // Search resault save
	//var fwd = Vector3.forward; // Foward of Turret
	var fwd = transform.TransformDirection(Vector3.forward); // Global 
	
	// If search fail
	Debug.DrawRay(spPoint.transform.position, fwd * 20, Color.green); // Distance Show
	if(Physics.Raycast(spPoint.transform.position, fwd, hit, 20) == false) return;
	if(hit.collider.gameObject.tag != "TANK" || ftime < 2) return; // Time check
	Debug.Log(hit.collider.gameObject.name); // Searching Object Show
	
	if(hit.collider.gameObject.tag != "TANK") return;
	// spawnPoint on explosion
	Instantiate(explosion, spPoint.transform.position, Quaternion.identity);
	
	//Bullet
	var obj = Instantiate(bullet, spPoint.transform.position, spPoint.transform.rotation);
	obj.rigidbody.AddForce(fwd * power);
	
	AudioSource.PlayClipAtPoint(snd, spPoint.transform.position); // Bullet Sound
	ftime = 0; // Time reset
}