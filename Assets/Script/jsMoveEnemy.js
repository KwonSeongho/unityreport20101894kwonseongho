#pragma strict

var rotAng = 15; // for Second rotate angle
function Update () {
	var amtToRot = rotAng * Time.deltaTime;
	transform.RotateAround(Vector3.zero, Vector3.up, amtToRot);
}