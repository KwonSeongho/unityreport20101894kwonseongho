#pragma strict

var snd : AudioClip; // sound file
var explosion : Transform; // explosion object
var random : int; //add critical hit
static var criticalHit : int; // boolen ciriticalHit

function Update()
{

}

function OnTriggerEnter(coll : Collider) {
		Instantiate(explosion, coll.transform.position, Quaternion.identity); //particle direction is Center of WALL 
		AudioSource.PlayClipAtPoint(snd, transform.position); // sound output
		Destroy(gameObject); //bullet destroy
		
		if(coll.gameObject.tag == "WALL") { // if collider tag are "WALL"
			Destroy(coll.gameObject); // WALL destroy
		}else if (coll.gameObject.tag == "ENEMY"){
			random = Random.Range(0,100); // range random
			if(random <= 90)
			{
			criticalHit = 0;
			}else
			{
			criticalHit = 1;
			}
			if(criticalHit == 0) // if random <= 90 
			{
			jsScore.hit = jsScore.hit + 1 * jsScore.Power; // one shot
			}else if(criticalHit == 1)
			{
			jsScore.hit = (jsScore.hit + 1 * jsScore.Power) * 2; // two shot
			}
			if(jsScore.hit > 5) { //if hit are over 5
				Destroy(coll.transform.root.gameObject); //EnemyTank Destroy
				Application.LoadLevel("WinGame");
			}
		}else if (coll.gameObject.tag == "TANK"){
			jsScore.lose++; // lose the score
			if(jsScore.lose > 5){
				Application.LoadLevel("LostGame");
			}
		}
}
