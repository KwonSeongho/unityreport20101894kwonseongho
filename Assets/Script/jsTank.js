#pragma strict

var speed = 5; //Move Speed
var rotSpeed = 120; // Rotate Speed
var turret : GameObject; //Turret Object

var power = 600; // Turret Fire Speed
var bullet : Transform; //Bullet
var explosion : Transform; // shot Explosion

var snd : AudioClip; // Shot Sound

function Update () {
	var amtToMove = speed * Time.deltaTime; //Frame move distance
	var amtToRot = rotSpeed * Time.deltaTime; // Frame rotate angle
	
	var front = Input.GetAxis("Vertical"); // Foward Back(Vector)
	var ang = Input.GetAxis("Horizontal"); // Left Right RotateDirection(Vector)
	var ang2 = Input.GetAxis("MyTank"); // Turret RotateDirection(Vector)
	
	transform.Translate(Vector3.forward * front * amtToMove); // Foward Back
	transform.Rotate(Vector3.up * ang * amtToRot); //Tank Rotate
	turret.transform.Rotate(Vector3.up * ang2 * amtToRot); //Turret Rotate
	
	//Bullet Fire
	if(Input.GetButtonDown("Fire1")){
		var spPoint = GameObject.Find("spawnPoint"); //spawnPoint information read
		Instantiate(explosion, spPoint.transform.position, Quaternion.identity); // shot Explosion direction
		AudioSource.PlayClipAtPoint(snd, spPoint.transform.position);// Shot Sound
		
		
		//var myBullet = Instantiate(bullet, spPoint.transform.position, spPoint.transform.rotation); //Euler
		var myBullet = Instantiate(bullet, spPoint.transform.position, Quaternion.identity); //Quaternion
		myBullet.rigidbody.AddForce(spPoint.transform.forward * power);
	}
}

